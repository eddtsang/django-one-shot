from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    all_list = TodoList.objects.all()
    context = {"all_todo_list": all_list}
    return render(request, "todolists/lists.html", context)


def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {"a_todo_list": list_detail}
    return render(request, "todolists/list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", new_list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todolists/create.html", context)


def todo_list_update(request, id):
    list_to_edit = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list_to_edit)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list_to_edit.id)
    else:
        form = TodoListForm(instance=list_to_edit)
    context = {"a_todo_list": list_to_edit, "form": form}
    return render(request, "todolists/edit.html", context)


def todo_list_delete(request, id):
    list_to_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_to_delete.delete()
        return redirect("todo_list_list")

    return render(request, "todolists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", new_item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todolists/item_create.html", context)


def todo_item_update(request, id):
    list_item_to_edit = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=list_item_to_edit)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list_item_to_edit.list.id)
    else:
        form = TodoItemForm(instance=list_item_to_edit)
    context = {"list_item": list_item_to_edit, "form": form}
    return render(request, "todolists/item_edit.html", context)
